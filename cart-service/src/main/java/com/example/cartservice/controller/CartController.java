package com.example.cartservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.cartservice.exception.CartNotFoundException;
import com.example.cartservice.model.Cart;
import com.example.cartservice.repository.CartRepository;
import com.example.cartservice.service.PricingService;

@RestController
public class CartController {
	
	@Autowired
	private CartRepository cartRepository;
	
	@Autowired
    private PricingService pricingService;
	
	@GetMapping("/cart/{id}")
	public Cart getCart(@PathVariable Integer id) {
		return cartRepository.findById(id).orElseThrow(() -> new CartNotFoundException("Cart not found:" + id));
	}
	
	@PostMapping("/cart")
    public Cart saveCart(@RequestBody Cart cart){

		Cart priced = pricingService.price(cart);
        Cart saved = cartRepository.save(priced);
        return saved;
    }
}
