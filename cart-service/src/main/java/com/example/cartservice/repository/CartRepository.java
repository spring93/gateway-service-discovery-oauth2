package com.example.cartservice.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.cartservice.model.Cart;

public interface CartRepository extends CrudRepository<Cart, Integer> {

}
