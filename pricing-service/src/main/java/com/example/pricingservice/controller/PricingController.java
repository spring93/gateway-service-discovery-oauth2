package com.example.pricingservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.pricingservice.model.Cart;
import com.example.pricingservice.service.PricingService;

@RestController
public class PricingController {
	
	@Autowired
    private PricingService pricingService;

    @PostMapping("/pricing/price")
    public Cart price(@RequestBody Cart cart){

        Cart priced = pricingService.price(cart);
        return priced;
    }
}
