package com.example.pricingservice.service;

import com.example.pricingservice.model.Cart;

public interface PricingService {
	Cart price(Cart cart);
}
